class Watcher {
    constructor(data, fn, mode = true) {
        this.nextData;
        this.data = data;
        this.onpush = mode;
        this.render(fn);
    }

    set changeData(data) {
        this.nextData = data;
    }

    render(fn) {
        let count = 0;
        fn(this.data, ++count);
        setInterval(() => {
            if (!this.nextData || this.onpush && this.nextData === this.data) return;
            this.data = this.nextData;
            fn(this.data, ++count);
        });
    }
}

const arrayAsEntities = (arr) => arr.reduce((acc, item) => ({ ...acc, [item.id]: item }), {});
