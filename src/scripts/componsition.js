/**
 * Easy composition
 */



function rabat(val) {
    return val * .9;
}
function vat(val) {
    return val * .23;
}
function additionalCharge(val) {
    return val + 10;
}

const compose = (a, b, c) => (initValue) => c(b(a(initValue)));

/**
 * Mało elegancki kod
 */
const result1 = rabat(vat(additionalCharge(50)));
console.log(result1);

/**
 * Uzycie funkcji compose wygląda już trochę lepiej
 */
const result2 = compose(additionalCharge, vat, rabat)(50);
console.log(result2);

/**
 * Example 2
 */

const compose2 = (...fns) => fns.reduceRight((prev, curr, i) => (...args) => prev(curr(...args)));

const buyProducts = (user, products) => {
    return { ...user, cart: [...user.cart, ...products] };
}

const addVat = (user) => {
    const vat = 1.23;
    const updatedCart = user.cart.map((item) => ({ ...item, price: item.price * vat }))
    return { ...user, cart: updatedCart };
}
const discount = (user) => {
    const updatedCart = user.cart.map((item) => ({ ...item, price: item.price * user.rabat }))
    return { ...user, cart: updatedCart };
}
const plus5 = (user) => {
    const promotion = .95;
    const updatedCart = user.cart.map((item) => ({ ...item, price: item.price * promotion }))
    return { ...user, cart: updatedCart };
}

const products = [
    { name: 'laptop', price: 1000 },
    { name: 'disk', price: 500 }
];

const initState = { name: 'Kris', rabat: .9, cart: [] };
const composeShoppingPrice = compose2(
    buyProducts,
    addVat,
    discount,
    plus5
)
const result = composeShoppingPrice(initState, products);

document.body.innerHTML += `
            <dl>
                <dt>before</dt>
                <dd>
                    <pre>${JSON.stringify(initState, null, 4)}</pre>
                </dd>
                <dt>after</dt>
                <dd>
                    <pre>${JSON.stringify(result, null,4)}</pre>
                </dd>
            </dl>
        `
